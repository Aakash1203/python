# 1) Type1
x = 10
y = 20
print(x)	# 10
print()		# 	
print(y)	# 20

# 2) Type2
print('Core2Web')		# Core2Web
print("Core2Web")		# Core2Web
print('''Core2Web''')		# Core2Web

print('Core2Web'+'Incubator')	# Core2WebIncubator
print('Core2Web','Incubator')	# Core2Web Incubator
print(3*'Biencaps')		# BiencapsBiencapsBiencaps 

# 3) Type3 (Seperator)

x = 10 
y = 20

print(x,y)			# 10 20
print(x,y,sep = ':')		# 10:20
print(x,y,sep = ',')		# 10,20
print(x,y,sep = '===')		# 10===20
print(x,end = " ")		# bcoz of end new line la jat nhi 
print(y)			# 10 20

# 4) Type4 (Formatted Output)(%d, %i, %f) like c/cpp

x = 10
y = 20.5

print("Value of x = %d " %x)	# 10
print("Value of x = %i " %x)	# 10
print("Value of y = %f " %y)	# 20.5

#print("Value of x = %i " x)	# 10

print("Value of x ={} and Value of y ={}".format(x,y))


