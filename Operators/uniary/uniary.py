

# ( + , -)

# 1)

x = 10
print(x)	# 10
ans = -x	
print(ans)	# -10

# 2)

x = 10
y = 20
print(++x)	# 10
#print(x++)	# SyntaxError: invalid syntax
print(x++y)	# 15
