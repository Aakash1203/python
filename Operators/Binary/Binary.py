
# 1)Arithmetic (+,-,*,/,%,//,**)

x = 2
y = 5

print(x+y)	# 7 
print(x-y)	# -3
print(x*y)	# 10
print(x/y)	# 0.4
print(x%y)	# 2
print(x//y)	# 0
print(x**y)	# 32 (2^5)


# 2)Relational (<,>,<=,>=,==,!=)  (Boolean value )

x = 10
y = 20

print(x<y)	# True
print(x>y)	# False
print(x<=y)	# True
print(x>=y)	# False
print(x==y)	# False
print(x!=y)	# True


# 3)Logical (and , or , not)   (Boolean Value)

x = True 
y = False

print(x and y)	# False
print(x or y)	# True
print(not y)	# True


a = 10
b = 20 

print(a and b)	# 20
print(a or b)	# 10
print(not b)	# False


a = 0
b = 20		# python madhe 0 ntr True value consider kelya jata	
	

print(a and b)	# 0
print(a or b)	# 20
print(not b)	# True




# 4)Assignment (+=,-=,/=,%=,*=)


x = 10
y = 5

x+=y
print(x)	# 15 (x = x + y)
x-=y
print(x)	# 15
x/=y
print(x)	# 2.0
x%=y
print(x)	# 2.0
x*=y
print(x)	# 10



# 5)Idetntity (is , is not)

x = 10
y = 10
z = 20


print(id(x))		# 4495186512 
print(id(y))		# 4495186512
print(id(z))		# 4495186832

print(x is y)		# True 
print(x is not y)	# false


# 6)Membership (in , not in)

listData = [10,20,30,40,50]

x = 10
print(x in listData)		# True 	
print(x not in listData)	# False



# 7) Bitwise (AND(&) , OR(|) , XOR(^), Leftshift(<<), Rightshift(>>), Negation/not(~))
# 	     (2^8, 2^7, 2^6, 2^5, 2^4, 2^3, 2^2, 2^1, 2^0)
#	     (256, 128, 64,  32,  16,  8,   4,   2,   1)
	
x = 10 		# 0 0 1 0 1 0
y = 20		# 0 0 0 1 0 1

print(x&y)	# 0 0 0 0 0 0 => 0
print(x|y)	# 0 0 1 1 1 1 => 30
print(x^y)	# 0 0 1 1 1 1 => 30
print(x<<2)	# 1 0 1 0     => 40
print(x>>2)	#     0 0 1 0 => 2
print(~x)	# -11 


