
# Dictonary Type/ Map Type 

player = {7:"Man",18:"Virat",45:"Rohit"}

print(player)		# {7: 'Man', 18: 'Virat', 45: 'Rohit'}
print(type(player))	# <class 'dict'>

player[7] = "Ms Dhoni"
print(player)		# {7: 'Ms Dhoni', 18: 'Virat', 45: 'Rohit'}
