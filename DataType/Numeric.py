
#(1) int Type
# i)
empId = 20
print(empId)   	   # 20
print(type(empId)) # <class 'int'> 
 
# ii)
jerNo = 7
print(jerNo)	   # 7	
print(type(jerNo)) #<class 'int'>


#(2) float Type 
# i)
price = 75.50
print(price)	   # 75.5
print(type(price)) #<class 'float'>

# ii)
data = 45.122333444455555666666777777788888888999999999
print(data)	  # 45.122333444455556   = 15 no. after decimal
print(type(data)) # <class 'float'>

#(3) Complex Number
complexdata = 10 + 5j
print(complexdata)	  #(10+5j)  = a+bi
print(type(complexdata))  #<class 'complex'>
