

# 1) String

# i)

friend1 = "Kanha"
print(friend1)		#Kanha
print(type(friend1))	# <class 'str'>

# ii)

friend2 = 'Ashish'
print(friend2)		#Ashish
print(type(friend2))	# <class 'str'>

# iii)

friend3 = '''Aakash'''
print(friend3)		#Aakash
print(type(friend3))	# <class 'str'>

# iv)

friend4 = """Patil"""	
print(friend4)		#Patil
print(type(friend4))	# <class 'str'>


# 2) List Type []

emplist = [18,"Ashish",25.5,'Patil',23,"Aakash"]
print(emplist)		#[18, 'Ashish', 25.5, 'Patil', 23, 'Aakash']
print(type(emplist))	#<class 'list'>
emplist[4] = "Anant"
print(emplist)		#[18, 'Ashish', 25.5, 'Patil', 'Anant', 'Aakash']


# 3) Tuple Type ()

empTuple = (18,"Ashish",25.5,'Patil',23,"Aakash")
print(empTuple)		#(18,"Ashish",25.5,'Patil',23,"Aakash")
print(type(empTuple))
#empTuple[4] = "Anant"	# error
#print(empTuple)		# TypeError: 'tuple' object does not support item assignment



# 4) Range Type  (0,1)

x = range(1,11)
print(x)		#range(1, 11)
print(type(x))		#<class 'range'>

y = 10
for i in x:
	print(i)	# print number 1 - 10





