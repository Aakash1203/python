
# 1) 

num = int(input("Enter Number : "))

if(num > 0):
	print("{} is Positive ".format(num))

elif(num < 0):		  # or else if
	print("{} is Negative".format(num))

else:
	print("{} is Zero ".format(num));


# 2)

mark = int(input("Enter The Marks : "))

if(mark > 90):
	print("{} Pass with A+ ".format(mark))
elif(mark > 80):
	print("{} Pass with A ".format(mark)) 
elif(mark > 70):
        print("{} Pass with B+ ".format(mark))
elif(mark > 60):
        print("{} Pass with B ".format(mark))
elif(mark > 40):
        print("{} Pass with c ".format(mark))
else:
        print("{} Fail ".format(mark))
