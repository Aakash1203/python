# 1)

x = int(input("Enter The value of x : "))

if x > 20:

    print("x is greater than 20")

else:

    print("x is less than 20")



# 2) Error = IndentationError
#    condition ntr statements both are must be in same line 
'''
x = int(input("Enter The value of x : "))

if x > 20:
    print("x is greater than 20")
else:
	    print("x is less than 20")	# IndentationError: unindent does not match any outer indentation level
	print("In else Block")

'''


# 3) {} = chlt but doni same line vr pahije 


x = int(input("Enter The value of x : "))

if x > 20:
    print("x is greater than 20")
else:
    print("x is less than 20")
    {
	print("In else Block")	
    }



# 4) Find Even and Odd

x = int(input("Enter The Number  : "))

if(x%2==0):
    print("Given Number is Even")	# or  print("%d is Even " %x)
else:
    print("%d is Odd " %x)
