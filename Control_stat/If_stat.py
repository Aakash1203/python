# Error = SyntaxError
'''
if (age > 18) 		# SyntaxError: invalid syntax

    print("Eligible for voting")
'''



# 1) 
age = int(input("Enter Your Age : "))

if(age>18):
	print("Elgible for Voting")
print("Out Side If")


# 2) Error = IndentationError
'''
age = int(input("Enter Your Age : "))

if(age>18):
print("Elgible for Voting")	# IndentationError: expected an indented block
print("Out Side If")

'''

# 3)
x = 10
y = 20

if(x):			# (x > 0) hence true
   print("True")
print("Out of if")
